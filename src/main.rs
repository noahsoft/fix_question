extern crate clap;
use clap::{Arg, App};

use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, Error, Read, Write};
use std::str::FromStr;

// 正则表达式组件
use regex::{Regex, Captures};
// JSON 组件
use serde::{Deserialize, Serialize};
// use serde_json::Result;

#[derive(Serialize, Deserialize)]
struct Question {
    id: String,
    r#type: String,
    section: String,
    title: String,
    options: String,
    answer: String,
    imgs: String,
    answer_imgs: String,
    analysis: String,
    status: String,
}

#[derive(Serialize, Deserialize)]
struct Questions {
    items: Vec<Question>,
}

struct InputArgs {
    file: String,
    topic_type: String,
}

fn get_args() -> InputArgs {
    let matches = App::new("安全生产试题整理工具")
    .version("0.1.0")
    .author("Marmot <xiaodong.wan@i-tudou.com>")
    .about("用于安全生产考试试题转换成规范的JSON数据格式，用于App直接使用")
    .arg(Arg::new("file")
             .short('f')
             .long("file")
             .takes_value(true)
             .required(true)
             .about("要转换的原始试题文件"))
    .arg(Arg::new("type")
             .short('t')
             .long("type")
             .takes_value(true)
             .about("试题类型（单选题、判断题、多选题）"))
    .get_matches();

    // 获取输入文件参数，必须
    let q_file = matches.value_of("file").unwrap();
    println!("文件: {}", q_file);
    // 获取类型参数，可选，默认“单选题”
    let q_type = matches.value_of("type").unwrap_or("单选题");
    println!("类型: {}", q_type);

    // 返回参数对象
    InputArgs {file: q_file.to_string(), topic_type: q_type.to_string()}
}

/// 原始试题文本，根据空行，添加每个试题的分隔符$，方便验证和后面拆分迭代
fn add_separator(file: &str) -> Result<String, std::io::Error> {
    // 读取文件 -> 字符串中 -> 拆分字符串 -> 解析
    let mut input = File::open(file)?;
    let mut buffered = String::new();
    input.read_to_string(&mut buffered)?;

    // 空行正则：Windows平台上：\r\n[\s| ]*\r\n
    let reg = Regex::new(r"\r\n[\s| ]*\r\n").unwrap();
    // 添加 $ 分隔符
    let context = reg.replace_all(&buffered, "\r\n$$\r\n").to_string();
    // 返回值
    Ok(context)
}

fn get_question(idx: usize, items: &str, topic_type: &str, group_tag: i32) -> Question {
    let item: Vec<&str> = items.split("\r\n").collect();
    let id = idx + 1;
    let section = format!("安全生产 - {}（{}）",topic_type ,group_tag);
    let title = item[0];
    let mut answer: String = "".to_string();
    let opt_a = item[1];
    let opt_b = item[2];
    let opt_c = item[3];
    let opt_d = item[4];
    let options = format!("{}\n{}\n{}\n{}", opt_a, opt_b, opt_c, opt_d);

    // 正则提取答案（注意添加了一个空格,兼容前后有空格的情况）
    let r = Regex::new(r"（([A-E ]+)）").unwrap();
    for cap in r.captures_iter(&title) {
        // println!("所有匹配结果：{}", &cap[0]);
        // println!("只匹配第一项：{}", &cap[1]);
        answer = format!("{}", &cap[1].trim());
        // 只需获取第一个答案即可（题型决定）
        break;
    }
    // 替换 答案 为 空中文字符
    let title = r.replace_all(&title, "（  ）").to_string();

    // 替换 试题开始序号
    let r2 = Regex::new(r"^\d+[、.]").unwrap();
    let title = r2.replace_all(&title, "").to_string();

    Question {
        id: id.to_string(), 
        title: title.to_string(),
        r#type: topic_type.to_string(), 
        section: section.to_string(), 
        answer: answer.to_string(), 
        options: options.to_string(), 
        imgs: "".to_string(), 
        answer_imgs: "".to_string(), 
        analysis: "".to_string(), 
        status: "1".to_string()
    }
}

fn main() -> Result<(), std::io::Error> {
    // 1、命令行参数处理
    let input_args = get_args();
    let file = input_args.file;
    let topic_type = input_args.topic_type;

    // 2、读取文件、解析文本，获取试题信息
    let context = add_separator(&file).unwrap();
    // 试题集合对象
    let mut questions = Questions{ items: Vec::new() };
    // 拆分 并 trim，返回字符串切片动态数组
    let q_iter: Vec<&str> = context.trim().split('$').map(|s| s.trim()).collect();
    let total = q_iter.len();
    println!("total：{}", total);

    let mut group_tag = 0;
    for (idx, items) in q_iter.iter().enumerate() {
        // 分组标签，25题一组，一次递增
        if idx % 25 == 0 {
            group_tag = group_tag + 1;
            println!("{}", group_tag);
        }

        let question = get_question(idx, items, &topic_type, group_tag);

        questions.items.push(question);
    }

    // 3、转换并输出文件
    let jsons = serde_json::to_string(&questions)?;
    // println!("{}", jsons);
    let mut output = File::create("test.json")?;
    write!(output, "{}", jsons)?;

    Ok(())
}
